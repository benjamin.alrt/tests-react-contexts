import { useUser } from '@/contexts/UserNewContext';
import { UserContext } from '@/contexts/UserProvider';
import { UserContextType } from '@/types/user';
import React, { Dispatch, SetStateAction, useContext } from 'react';

interface UserProps {
    user : string;
}

const UserWithProps = ({ user} : UserProps) => {
    return (
        <div>
            {user}
        </div>
    );
};

const UserWithContext = () => {
    const { user, setUser } = useContext(UserContext) as UserContextType;

    return (
        <div>
            {user}
        </div>
    );
};

const UserWithConstate = () => {

    const { user, setUser } = useUser();

    return (
        <div>
            {user}
        </div>
    );
};


export { UserWithProps, UserWithContext, UserWithConstate};