import { UserContextType } from "@/types/user";
import { createContext, useState } from "react";

export const UserContext = createContext<UserContextType | null>(null);

export const UserProvider = ({ children } : any) => {
    const [user, setUser] = useState('My super context user');
  
    return (
      <UserContext.Provider value={{ user, setUser }}>
        {children}
      </UserContext.Provider>
    );
  }
