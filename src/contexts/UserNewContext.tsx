import {useState} from 'react';
import constate from 'constate';

const userNewContext = () => {
	const [user, setUser] = useState('My super constate user');
  
  return {user, setUser};
};

export const [UserNewProvider, useUser] = constate(userNewContext);