export type UserContextType = {
    user: string;
    setUser: (user: string) => void;
  };