import { UserNewProvider } from '@/contexts/UserNewContext';
import { UserProvider } from '@/contexts/UserProvider';
import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import { useState } from 'react';

export default function App({ Component, pageProps }: AppProps) {

  return (
    <UserProvider>
      <UserNewProvider>
        <Component {...pageProps} />
      </UserNewProvider>
    </UserProvider>
  )
}
